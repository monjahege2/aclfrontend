import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {
  username: string;
  password: string;

  constructor() {
  }

  setCredentials(name: string, password: string): void {
    this.username = name;
    this.password = password;
  }

  getUsername(): string {
    return this.username;
  }


}
