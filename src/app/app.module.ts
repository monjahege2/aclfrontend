import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {CustomMaterialModule} from './core/material.module';
import {FormBuilder, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {AppRoutingModule} from './core/routing.module';
import {RouterModule} from '@angular/router';
import {VertraegeComponent} from './vertraege/vertraege.component';
import {MatChipsModule} from '@angular/material/chips';
import {FlexLayoutModule} from '@angular/flex-layout';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ShowVertragComponent} from './vertraege/show-vertrag/show-vertrag.component';
import {IdComponent} from './formComponents/id/id.component';
import {NameComponent} from './formComponents/name/name.component';
import {ProjektnummerComponent} from './formComponents/projektnummer/projektnummer.component';
import {NotizComponent} from './formComponents/notiz/notiz.component';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {AuthService} from './AuthService';
import {CredentialInterceptor} from './service/credential.interceptor';
import {FirstPageComponent} from './first-page/first-page.component';
import {MatButtonModule} from '@angular/material/button';
import {NewUserComponent} from './login/new-user/new-user.component';
import {UserComponent} from './login/user/user.component';
import {ShowUserComponent} from './login/show-user/show-user.component';
import {RoleComponent} from './formComponents/role/role.component';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import {NewVertragComponent} from './vertraege/new-vertrag/new-vertrag.component';
import {ForbiddenComponent} from './forbidden/forbidden.component';
import {NgxPermissionsModule} from 'ngx-permissions';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    VertraegeComponent,
    ShowVertragComponent,
    IdComponent,
    NameComponent,
    ProjektnummerComponent,
    NotizComponent,
    FirstPageComponent,
    NewUserComponent,
    UserComponent,
    ShowUserComponent,
    RoleComponent,
    NewVertragComponent,
    ForbiddenComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CustomMaterialModule,
    FormsModule,
    AppRoutingModule,
    RouterModule,
    MatChipsModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatCardModule,
    MatDividerModule,
    MatButtonModule,
    MatSelectModule,
    MatTooltipModule,
    MatDialogModule,
    NgxPermissionsModule.forRoot()
  ],
  providers: [
    FormBuilder,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: CredentialInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
