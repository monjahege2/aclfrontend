import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from '../login/login.component';
import {VertraegeComponent} from '../vertraege/vertraege.component';
import {ShowVertragComponent} from '../vertraege/show-vertrag/show-vertrag.component';
import {FirstPageComponent} from '../first-page/first-page.component';
import {NewUserComponent} from '../login/new-user/new-user.component';
import {UserComponent} from '../login/user/user.component';
import {ShowUserComponent} from '../login/show-user/show-user.component';
import {NewVertragComponent} from '../vertraege/new-vertrag/new-vertrag.component';
import {ForbiddenComponent} from '../forbidden/forbidden.component';

export const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'vertraege', component: VertraegeComponent},
  { path: '', component: FirstPageComponent},
  { path: 'vertrag/:id', component: ShowVertragComponent},
  { path: 'newUser', component: NewUserComponent},
  { path: 'users', component: UserComponent},
  { path: 'user/:id', component: ShowUserComponent},
  { path: 'newVertrag', component: NewVertragComponent},
  { path: 'forbidden', component: ForbiddenComponent}
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
