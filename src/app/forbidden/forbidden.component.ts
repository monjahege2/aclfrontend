import {Component, OnInit} from '@angular/core';
import {AuthService} from '../AuthService';
import {BackendService} from '../service/BackendService';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.css']
})
export class ForbiddenComponent implements OnInit {

  url = { url: '', navigationId: ''};

  constructor(private backendService: BackendService,
              private route: ActivatedRoute,
              private router: Router,
              private auth: AuthService,
              private _location: Location) { }

  ngOnInit(): void {
    this.url = history.state;
  }

  logout() {
    this.auth.setCredentials('', '');
    this.router.navigateByUrl('/login');
  }

  back(): void {
    this.router.navigateByUrl(this.url.url);
  }

}
