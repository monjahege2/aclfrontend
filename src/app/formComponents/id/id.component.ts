import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-id',
  templateUrl: './id.component.html',
  styleUrls: ['./id.component.css']
})
export class IdComponent implements OnInit {

  @Input() control: AbstractControl;

  constructor() { }

  ngOnInit(): void {
  }

  getControlNameMethod(control: AbstractControl): string | null {
    const formGroup = control.parent.controls;

    return Object.keys(formGroup).find(name => control === formGroup[name]) || null;
  }

}
