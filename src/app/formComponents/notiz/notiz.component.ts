import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl} from '@angular/forms';

@Component({
  selector: 'app-notiz',
  templateUrl: './notiz.component.html',
  styleUrls: ['./notiz.component.css']
})
export class NotizComponent implements OnInit {

  @Input() control: AbstractControl;

  constructor() { }

  ngOnInit(): void {
  }

  getControlNameMethod(control: AbstractControl): string | null {
    const formGroup = control.parent.controls;

    return Object.keys(formGroup).find(name => control === formGroup[name]) || null;
  }

}
