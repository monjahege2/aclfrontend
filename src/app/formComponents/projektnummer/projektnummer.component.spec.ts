import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjektnummerComponent } from './projektnummer.component';

describe('ProjektnummerComponent', () => {
  let component: ProjektnummerComponent;
  let fixture: ComponentFixture<ProjektnummerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjektnummerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjektnummerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
