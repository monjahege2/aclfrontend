import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, Validators} from '@angular/forms';
import {BackendService} from '../../service/BackendService';


@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {

  @Input() control: AbstractControl;
  roleControl = new FormControl(this.control, Validators.required);
  selected = 'initialRole';
  roles: String[];
  controlValue: string;
  disabled: boolean;

  constructor(private backendService: BackendService) { }

  ngOnInit(): void {
    this.controlValue = this.control.value;
    this.disabled = this.control.disabled;
    this.backendService.getAllRoles().subscribe( (roles: String[]) => {
      this.roles = roles;
      this.editRoles();
    });
  }

  editRoles(): void {
    const roleNumber = this.roles.indexOf(this.control.value);
    if (roleNumber >= 0) {
      this.roles.splice(roleNumber, 1);
    }
  }

  getRole(): string {
    return this.controlValue;
  }

  getInnerText(innerText): void {
    if (this.control.value !== innerText) {
      this.control.setValue(innerText);
    }
  }



}
