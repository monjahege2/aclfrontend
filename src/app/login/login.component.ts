import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../AuthService';
import {HttpClient} from '@angular/common/http';
import {BackendService} from '../service/BackendService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private http: HttpClient, private router: Router,
              private backendService: BackendService) { }

  credentials = {username: null, password: null};
  authenticate: boolean;

  ngOnInit() {
  }

  login() {
    this.authService.setCredentials(this.credentials.username, this.credentials.password);
    this.backendService.getUserByName(this.credentials.username).subscribe( () => {
        this.authenticate = true;
        this.router.navigateByUrl('/vertraege');
      }, () => {
      this.authenticate = false;
      this.credentials.username = null;
      this.credentials.password = null;
      },
    );
  }

  checkFields(): boolean {
    return this.credentials.password == null || this.credentials.username == null ||
      this.credentials.password === '' || this.credentials.username === '';
  }
}

