import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../AuthService';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import {BackendService} from '../../service/BackendService';
import {User} from '../../models/User';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  constructor( private authService: AuthService, private http: HttpClient,
               private router: Router, private backendService: BackendService) { }

  username: string;

  passwordNew: string;
  confirmation: string;

  falsePasswort: boolean;

  roles: String[];

  newUser: User = new class extends User {};

  ngOnInit(): void {
    (this.backendService.saveUser(this.newUser)).subscribe(() => {}, (error: HttpErrorResponse) => {
      if (error.status === 403) {
        this.router.navigate(['/forbidden'], {state: {url: '/vertraege'}});
      }
      }
    );

    this.backendService.getAllRoles().subscribe( (roles: String[]) => {
      this.roles = roles;
    });
  }

  save() {
    if (this.passwordNew !== this.confirmation) {
      this.passwordNew = null;
      this.confirmation = null;
      this.falsePasswort = true;
    } else {
      this.newUser.name = this.username;
      this.newUser.passwort = this.passwordNew;
      this.backendService.saveUser(this.newUser).subscribe();
      this.router.navigateByUrl('/users');
    }
  }

  back(): void {
    this.router.navigate(['/users']);
  }

  getInnerText(innerText): void {
      this.newUser.role = innerText;
  }

  checkFields(): boolean {
    if ( this.passwordNew == null || this.passwordNew === '' || this.confirmation == null ||
      this.confirmation === '' || this.newUser.role == null || this.username == null || this.username === '') {
      return true;
    }
  }

}
