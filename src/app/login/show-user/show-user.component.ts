import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../../service/BackendService';
import {User} from '../../models/User';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  private routeSub: Subscription;
  user: User;
  public userForm: FormGroup;
  private id: number;
  private changed;
  private hasPermission: boolean;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private backendService: BackendService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.backendService.getUserById(this.id).subscribe(user => {
      this.user = user;
      this.backendService.saveUser(this.user).subscribe(() => {
        this.hasPermission = true;
        this.buildForm();
      }, error => {
        this.hasPermission = false;
        this.buildForm();
      });
    });
  }

  buildForm(): void {
    this.userForm = this.fb.group( {
      id: {value: this.user.id, disabled: true},
      name: { value: this.user.name, disabled: !this.hasPermission},
      passwort: { value: this.user.passwort, disabled: !this.hasPermission},
      role: { value: this.user.role, disabled: !this.hasPermission}
    });
    this.subscribeToFormChanges();
  }

  isChanged(): boolean {
    return this.changed;
  }

  subscribeToFormChanges(): void {
    this.userForm.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }


  save(): void {
    const userToSave = this.userForm.getRawValue();
    this.backendService.saveUser(userToSave).subscribe();
    this.changed = false;
  }

  delete(): void {
    this.backendService.deleteUserById(this.user.id).subscribe();
    this.router.navigate(['/users/']);
  }

  back(): void {
    this.router.navigate(['/users']);
  }

}
