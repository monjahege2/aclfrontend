import {Component, OnInit} from '@angular/core';
import {BackendService} from '../../service/BackendService';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../AuthService';
import {User} from '../../models/User';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  users: User[];

  constructor(
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService) {
  }

  ngOnInit(): void {
    (this.backendService.getAllUser()).subscribe((users: User[]) => {
      this.users = users;
      }, () =>  this.router.navigate(['/forbidden'], {state: { url: '/vertraege'}})
    );
  }

  showUser(id: number): void {
    this.router.navigate(['/user/' + id]);
  }

  newUser() {
    this.router.navigateByUrl('/newUser');
  }

  logout() {
    this.auth.setCredentials('', '');
    this.router.navigateByUrl('/login');
  }

  showVertraege() {
    this.router.navigate(['/vertraege']);
  }

}
