import {BackendServiceDefinition} from './BackendServiceDefintion';
import {Vertrag} from '../models/Vertrag';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {User} from '../models/User';
import {Projekt} from '../models/Projekt';
import {catchError} from 'rxjs/operators';
import {WriteType} from '../models/WriteType';

@Injectable({
  providedIn: 'root'
})
export class BackendService implements BackendServiceDefinition {

  constructor(private http: HttpClient) {
  }

  url = 'http://localhost:8080';

  getVertragById(id: number): Observable<Vertrag> {
    return this.http.get<Vertrag>(`${this.url}/vertrag/${id}`);
  }

  getAllVertraege(): Observable<Vertrag[]> {
    return this.http.get<Vertrag[]>(`${this.url}/vertrag`);
  }

  deleteVertragById(id: number): Observable<Object> {
    return this.http.delete(`${this.url}/vertrag/${id}`);
  }

  saveVertrag(vertrag: Vertrag): Observable<Vertrag> {
    return this.http.post<Vertrag>(`${this.url}/vertrag`, vertrag);
  }

  updateVertrag(vertrag: Vertrag): Observable<Vertrag> {
    return this.http.put<Vertrag>(`${this.url}/vertrag`, vertrag);
  }

  getAllUser(): Observable<User[]> {
    return this.http.get<User[]>(`${this.url}/user`);
  }

  getUserById(id: number): Observable<User> {
    return this.http.get<User>(`${this.url}/user/${id}`);
  }

  saveUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.url}/user`, user);
  }

  deleteUserById(id: number): Observable<Object> {
    return this.http.delete(`${this.url}/user/${id}`);
  }

  getAllRoles(): Observable<String[]> {
    return this.http.get<String[]>(`${this.url}/roles`);
  }

  getProjektById(id: number): Observable<Projekt> {
    return this.http.get<Projekt>(`${this.url}/projekt/${id}`);
  }

  getAllProjekte(): Observable<Projekt[]> {
    return this.http.get<Projekt[]>(`${this.url}/projekt`);
  }

  getUserByName(name: string): Observable<User> {
    return this.http.get<User>(`${this.url}/userByName/${name}`).pipe(catchError(null ));
  }

  getAllProjekteByName(name: string): Observable<Projekt[]> {
    return this.http.get<Projekt[]>(`${this.url}/projektByName/${name}`);
  }

  getHasPermission(writeType: WriteType, ObjectId: Number ): Observable<Boolean> {

    return this.http.get<Boolean>(`${this.url}/hasPermission/${writeType}/${ObjectId}`);
  }
}
