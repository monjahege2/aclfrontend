import {Observable} from 'rxjs';
import {Vertrag} from '../models/Vertrag';

export interface BackendServiceDefinition {
  getVertragById(id: number): Observable<Vertrag>;
}
