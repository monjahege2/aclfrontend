import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewVertragComponent } from './new-vertrag.component';

describe('NewVertragComponent', () => {
  let component: NewVertragComponent;
  let fixture: ComponentFixture<NewVertragComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewVertragComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewVertragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
