import {Component, OnInit} from '@angular/core';
import {Vertrag} from '../../models/Vertrag';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {BackendService} from '../../service/BackendService';
import {Projekt} from '../../models/Projekt';
import {User} from '../../models/User';
import {AuthService} from '../../AuthService';
import {mergeMap} from 'rxjs/operators';
import {iif} from 'rxjs';

@Component({
  selector: 'app-new-vertrag',
  templateUrl: './new-vertrag.component.html',
  styleUrls: ['./new-vertrag.component.css']
})
export class NewVertragComponent implements OnInit {

  newVertrag: Vertrag = new class extends Vertrag {};
  vertragName: string;
  notiz: string;
  user: User;

  projekte: Projekt[];

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private backendService: BackendService,
    private router: Router,
    private auth: AuthService
  ) {
  }

  ngOnInit(): void {
    if ( this.auth.username === 'admin') {
      this.backendService.getAllProjekte().subscribe( projekte => this.projekte = projekte);
    } else {
      this.backendService.getUserByName(this.auth.username).pipe(
        mergeMap( user => iif(() => user.role.slice(5, user.role.length - 2) !== 'Projekt',
          this.backendService.getAllProjekte(),
          this.backendService.getAllProjekteByName(user.role.slice(5, user.role.length))))).subscribe(
        projekte => {
          this.projekte = projekte;
        });
    }
  }

  save() {
      this.newVertrag.name = this.vertragName;
      this.newVertrag.notiz = this.notiz;
      this.backendService.saveVertrag(this.newVertrag).subscribe();
      this.router.navigateByUrl('/vertraege');
    }

  back(): void {
    this.router.navigate(['/vertraege']);
  }

  getInnerText(innerText: Projekt): void {
    this.newVertrag.projekt = innerText.id;
  }

  checkFields(): boolean {
    if ( this.vertragName == null || this.vertragName === '' || this.newVertrag.projekt == null) {
      return true;
    }
  }
}
