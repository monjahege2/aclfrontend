import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowVertragComponent } from './show-vertrag.component';

describe('ShowVertragComponent', () => {
  let component: ShowVertragComponent;
  let fixture: ComponentFixture<ShowVertragComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowVertragComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowVertragComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
