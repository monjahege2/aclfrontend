import {Component, OnInit} from '@angular/core';
import {Vertrag} from '../../models/Vertrag';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {BackendService} from '../../service/BackendService';
import {Projekt} from '../../models/Projekt';
import {WriteType} from '../../models/WriteType';

@Component({
  selector: 'app-show-vertrag',
  templateUrl: './show-vertrag.component.html',
  styleUrls: ['./show-vertrag.component.css']
})
export class ShowVertragComponent implements OnInit {

  private routeSub: Subscription;
  vertrag: Vertrag;
  public vertragForm: FormGroup;
  private id: number;
  private changed;
  projekt: Projekt;
  vertragToSave: Vertrag = new class extends Vertrag {};
  private hasPermission: Boolean = false;

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private backendService: BackendService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.backendService.getHasPermission(WriteType.UPDATE, this.id).subscribe( hasPermission => {
      this.hasPermission = hasPermission;
    });

    this.backendService.getVertragById(this.id).subscribe(vertrag => {
      this.vertrag = vertrag;
      this.backendService.getProjektById(this.vertrag.projekt).subscribe(projekt => {
        this.projekt = projekt;
        this.buildForm();
      });
    } , () =>  this.router.navigate(['/forbidden'], {state: { url: '/vertraege'}}));
  }

  buildForm(): void {
    this.vertragForm = this.fb.group( {
      id: {value: this.vertrag.id, disabled: true},
      name: {value: this.vertrag.name, disabled: !this.hasPermission},
      projektnummer: { value: this.projekt.name, disabled: true},
      notiz:  {value: this.vertrag.notiz, disabled: !this.hasPermission}
    });
    this.subscribeToFormChanges();
  }

  isChanged(): boolean {
    return this.changed;
  }

  subscribeToFormChanges(): void {
    this.vertragForm.valueChanges.subscribe(() => {
      this.changed = true;
    });
  }


  save(): void {
    this.vertragToSave.name = this.vertragForm.get('name').value;
    this.vertragToSave.notiz = this.vertragForm.get('notiz').value;
    this.vertragToSave.id = this.vertragForm.get('id').value;
    this.vertragToSave.projekt = this.projekt.id;
    this.backendService.updateVertrag(this.vertragToSave).subscribe();
    this.changed = false;
  }

  delete(): void {
    this.backendService.deleteVertragById(this.vertrag.id).subscribe();
    this.router.navigate(['/vertraege/']);
  }

  back(): void {
    this.router.navigate(['/vertraege']);
  }

}
