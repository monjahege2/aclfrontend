import {Component, OnInit} from '@angular/core';
import {BackendService} from '../service/BackendService';
import {Vertrag} from '../models/Vertrag';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../AuthService';
import {NgxPermissionsService} from 'ngx-permissions';

@Component({
  selector: 'app-vertraege',
  templateUrl: './vertraege.component.html',
  styleUrls: ['./vertraege.component.css']
})
export class VertraegeComponent implements OnInit {

  vertraege: Vertrag[];
  perm = ['ROLE_ADMIN', 'ROLE_Projekt 1', 'ROLE_Projekt 2'];

  constructor(
    private backendService: BackendService,
    private route: ActivatedRoute,
    private router: Router,
    private auth: AuthService,
    private permissionsService: NgxPermissionsService) {
  }

  ngOnInit(): void {
    // used PermissionService but react on roles --> with a special hasCreatePermission you could ignore the roles
    this.permissionsService.flushPermissions();
    this.backendService.getUserByName(this.auth.username).subscribe(user => {
      this.permissionsService.addPermission(user.role);
    });


    (this.backendService.getAllVertraege()).subscribe((vertraege: Vertrag[]) => {
      this.vertraege = vertraege;
    });
  }

  showVertrag(id: number): void {
    this.router.navigate(['/vertrag/' + id]);
  }

  newVertrag() {
    this.router.navigateByUrl('/newVertrag');
  }

  logout() {
    this.auth.setCredentials('', '');
    this.router.navigateByUrl('/login');
  }

  showUser() {
    this.router.navigateByUrl('/users');
  }
}
